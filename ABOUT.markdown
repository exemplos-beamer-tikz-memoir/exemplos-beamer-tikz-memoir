O projeto
=========

É uma pequena coleção de exemplos de uso dos pacotes tikz, beamer e memoir. O intuito é apresentar exemplos simples o bastante para serem estudados por alguém com alguma familiaridade com LaTeX (talvez alguns meses de uso esporádico).

Por outro lado, esperamos que os exemplos sejam interessantes e sofisticados o suficiente para motivar alguma leitura dos manuais dos pacotes, e envolventes o bastante para que se possa dispender uma tarde agradável com eles.

É isso.